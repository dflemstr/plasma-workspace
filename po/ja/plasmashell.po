# Fuminobu Takeyama <ftake@geeko.jp>, 2016.
# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2020.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2023.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2016, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasmashell\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2023-04-07 23:52-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Fuminobu TAKEYAMA"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ftake@geeko.jp"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "マウスアクションプラグインを設定"

#: desktopview.cpp:211
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "Plasma シェル"

#: desktopview.cpp:214
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "Plasma シェル"

#: desktopview.cpp:217
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "Plasma シェル"

#: desktopview.cpp:220
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "Plasma シェル"

#: desktopview.cpp:223
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "Plasma シェル"

#: desktopview.cpp:226
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "Plasma シェル"

#: desktopview.cpp:252
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "Plasma シェル"

#: desktopview.cpp:256
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "Plasma シェル"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr ""

#: desktopview.cpp:263
#, fuzzy, kde-format
#| msgid "Plasma Shell"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "Plasma シェル"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr ""

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "Plasma シェル"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "QML JavaScript デバッガを有効にする "

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "クラッシュ後に plasma-shell を自動的に再起動しない"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr ""

#: main.cpp:110
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: main.cpp:113
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""

#: main.cpp:114
#, kde-format
msgid "file"
msgstr ""

#: main.cpp:118
#, kde-format
msgid "Lists the available options for user feedback"
msgstr ""

#: main.cpp:200
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Plasma の起動に失敗"

#: main.cpp:201
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"OpenGL 2 もソフトウェアによるフォールバックも使用できないため、Plasma を起動"
"できません。\n"
"グラフィックドライバが正しく設定されているか確認してください。"

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "オーディオミュート"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "マイクミュート"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 をミュート"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "タッチパッドオン"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "タッチパッドオフ"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi オン"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi オフ"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth オン"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth オフ"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "モバイルインターネットオン"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "モバイルインターネットオフ"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "オンスクリーンキーボードがアクティブ化されました"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "オンスクリーンキーボードが非アクティブになりました"

#: panelview.cpp:999
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr ""

#: panelview.cpp:1001
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr ""

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "%1 の内部スクリーン"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%3 の %1 %2"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "切断されたスクリーン %1"

#: shellcorona.cpp:198 shellcorona.cpp:200
#, kde-format
msgid "Show Desktop"
msgstr "デスクトップを表示"

#: shellcorona.cpp:200
#, kde-format
msgid "Hide Desktop"
msgstr "デスクトップを隠す"

#: shellcorona.cpp:216
#, kde-format
msgid "Show Activity Switcher"
msgstr "アクティビティスイッチャーを表示"

#: shellcorona.cpp:227
#, kde-format
msgid "Stop Current Activity"
msgstr "現在のアクティビティを停止"

#: shellcorona.cpp:235
#, kde-format
msgid "Switch to Previous Activity"
msgstr "前のアクティビティに切り替え"

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Next Activity"
msgstr "次のアクティビティに切り替え"

#: shellcorona.cpp:258
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "タスクマネージャのエントリ %1 をアクティベート"

#: shellcorona.cpp:285
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "デスクトップとパネルを管理..."

#: shellcorona.cpp:307
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "キーボードのパネルへのフォーカスを移動"

#: shellcorona.cpp:2052
#, kde-format
msgid "Add Panel"
msgstr "パネルを追加"

#: shellcorona.cpp:2094
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "空の%1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "使用中のソフトウェアレンダラ"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "使用中のソフトウェアレンダラ"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr ""

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "再度表示しない"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "パネルの数"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "パネルの数を数えます"
