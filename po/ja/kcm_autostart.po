# Translation of kcm_autostart into Japanese.
# This file is distributed under the same license as the kdebase package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2008.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2015-05-22 22:12-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/entry.qml:52
#, kde-format
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr ""

#: ui/entry.qml:58
#, kde-format
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr ""

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr ""

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr ""

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr ""

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "Add Script..."
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "スクリプトを追加..."

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Add Script..."
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "スクリプトを追加..."

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:146
#, fuzzy, kde-format
#| msgid "&Properties..."
msgctxt "@action:button"
msgid "See properties"
msgstr "プロパティ(&P)..."

#: ui/main.qml:154
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "削除(&R)"

#: ui/main.qml:167
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:170
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:173
#, kde-format
msgid "Pre-startup Scripts"
msgstr ""

#: ui/main.qml:176
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:184
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:185
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""

#: ui/main.qml:200
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "スクリプトを追加..."

#: ui/main.qml:220
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr ""

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr ""
