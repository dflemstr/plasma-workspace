# Spanish translations for plasma_applet_org.kde.plasma.lock_logout.po package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2014.
# SPDX-FileCopyrightText: 2014, 2015, 2019, 2020, 2021, 2022, 2023 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-19 01:53+0000\n"
"PO-Revision-Date: 2023-10-20 22:20+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "General"

#: contents/ui/ConfigGeneral.qml:33
#, kde-format
msgctxt ""
"Heading for a list of actions (leave, lock, switch user, hibernate, suspend)"
msgid "Show actions:"
msgstr "Mostrar acciones:"

#: contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Log Out"
msgstr "Cerrar sesión"

#: contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Shut Down"
msgstr "Apagar"

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Restart"
msgstr "Reiniciar"

#: contents/ui/ConfigGeneral.qml:53 contents/ui/data.js:5
#, kde-format
msgid "Lock"
msgstr "Bloquear"

#: contents/ui/ConfigGeneral.qml:59
#, kde-format
msgid "Switch User"
msgstr "Cambiar usuario"

#: contents/ui/ConfigGeneral.qml:65 contents/ui/data.js:46
#, kde-format
msgid "Hibernate"
msgstr "Hibernar"

#: contents/ui/ConfigGeneral.qml:71 contents/ui/data.js:39
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Reposo"

#: contents/ui/data.js:6
#, kde-format
msgid "Lock the screen"
msgstr "Bloquear la pantalla"

#: contents/ui/data.js:12
#, kde-format
msgid "Switch user"
msgstr "Cambiar usuario"

#: contents/ui/data.js:13
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "Iniciar una sesión paralela como un usuario distinto"

#: contents/ui/data.js:18
#, kde-format
msgid "Shutdown…"
msgstr "Apagar..."

#: contents/ui/data.js:19
#, kde-format
msgid "Turn off the computer"
msgstr "Apagar el equipo"

#: contents/ui/data.js:25
#, kde-format
msgid "Restart…"
msgstr "Reiniciar..."

#: contents/ui/data.js:26
#, kde-format
msgid "Reboot the computer"
msgstr "Reiniciar el equipo"

#: contents/ui/data.js:32
#, kde-format
msgid "Logout…"
msgstr "Salir de la sesión..."

#: contents/ui/data.js:33
#, kde-format
msgid "End the session"
msgstr "Terminar la sesión"

#: contents/ui/data.js:40
#, kde-format
msgid "Sleep (suspend to RAM)"
msgstr "Reposo (suspender en memoria)"

#: contents/ui/data.js:47
#, kde-format
msgid "Hibernate (suspend to disk)"
msgstr "Hibernar (suspender en disco)"

#~ msgid "Logout"
#~ msgstr "Salir de la sesión"

#~ msgid "Reboot"
#~ msgstr "Reiniciar"

#~ msgid "Shutdown..."
#~ msgstr "Apagar..."

#~ msgid "Logout..."
#~ msgstr "Salir de la sesión..."

#~ msgid "Do you want to suspend to disk (hibernate)?"
#~ msgstr "¿Desea suspender en disco (hibernar)?"

#~ msgid "Yes"
#~ msgstr "Sí"

#~ msgid "No"
#~ msgstr "No"

#~ msgid "Do you want to suspend to RAM (sleep)?"
#~ msgstr "¿Desea suspender en memoria (dormir)?"

#~ msgid "Leave"
#~ msgstr "Salir"

#~ msgid "Leave..."
#~ msgstr "Salir..."

#~ msgctxt "Heading for list of actions (leave, lock, shutdown, ...)"
#~ msgid "Actions"
#~ msgstr "Acciones"

#~ msgid "Suspend"
#~ msgstr "Suspender"

#~ msgid "Lock/Logout Settings"
#~ msgstr "Preferencias de bloqueo o de fin de sesión"

#~ msgid "SystemTray Settings"
#~ msgstr "Preferencias de la bandeja del sistema"
